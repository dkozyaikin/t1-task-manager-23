package ru.t1.dkozyaikin.tm.component;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozyaikin.tm.api.repository.ICommandRepository;
import ru.t1.dkozyaikin.tm.api.repository.IProjectRepository;
import ru.t1.dkozyaikin.tm.api.repository.ITaskRepository;
import ru.t1.dkozyaikin.tm.api.repository.IUserRepository;
import ru.t1.dkozyaikin.tm.api.service.*;
import ru.t1.dkozyaikin.tm.command.AbstractCommand;
import ru.t1.dkozyaikin.tm.command.project.*;
import ru.t1.dkozyaikin.tm.command.system.*;
import ru.t1.dkozyaikin.tm.command.task.*;
import ru.t1.dkozyaikin.tm.command.user.*;
import ru.t1.dkozyaikin.tm.enumerated.Role;
import ru.t1.dkozyaikin.tm.enumerated.Status;
import ru.t1.dkozyaikin.tm.exception.AbstractException;
import ru.t1.dkozyaikin.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.dkozyaikin.tm.exception.system.CommandNotSupportedException;
import ru.t1.dkozyaikin.tm.model.Project;
import ru.t1.dkozyaikin.tm.model.Task;
import ru.t1.dkozyaikin.tm.model.User;
import ru.t1.dkozyaikin.tm.repository.CommandRepository;
import ru.t1.dkozyaikin.tm.repository.ProjectRepository;
import ru.t1.dkozyaikin.tm.repository.TaskRepository;
import ru.t1.dkozyaikin.tm.repository.UserRepository;
import ru.t1.dkozyaikin.tm.service.*;
import ru.t1.dkozyaikin.tm.util.TerminalUtil;

public final class  Boostrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    @Getter
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    @Getter
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    @Getter
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    @Getter
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    @Getter
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    @Getter
    private final IUserService userService = new UserService(userRepository, taskRepository, projectRepository);

    @NotNull
    @Getter
    private final IAuthService authService = new AuthService(userService);

    {
        try {
            registry(new ApplicationAboutCommand());
            registry(new ApplicationHelpCommand());
            registry(new ApplicationVersionCommand());
            registry(new CommandListCommand());
            registry(new ArgumentListCommand());
            registry(new ApplicationExitCommand());

            registry(new ProjectCreateCommand());
            registry(new ProjectListCommand());
            registry(new ProjectShowByIdCommand());
            registry(new ProjectShowByIndexCommand());
            registry(new ProjectUpdateByIdCommand());
            registry(new ProjectUpdateByIndexCommand());
            registry(new ProjectRemoveByIdCommand());
            registry(new ProjectRemoveByIndexCommand());
            registry(new ProjectClearCommand());
            registry(new ProjectStartByIdCommand());
            registry(new ProjectStartByIndexCommand());
            registry(new ProjectCompleteByIdCommand());
            registry(new ProjectCompleteByIndexCommand());
            registry(new ProjectChangeStatusByIdCommand());
            registry(new ProjectChangeStatusByIndexCommand());

            registry(new TaskCreateCommand());
            registry(new TaskListCommand());
            registry(new TaskClearCommand());
            registry(new TaskShowByIdCommand());
            registry(new TaskShowByIndexCommand());
            registry(new TaskUpdateByIdCommand());
            registry(new TaskUpdateByIndexCommand());
            registry(new TaskRemoveByIdCommand());
            registry(new TaskRemoveByIndexCommand());
            registry(new TaskStartByIdCommand());
            registry(new TaskStartByIndexCommand());
            registry(new TaskCompleteByIdCommand());
            registry(new TaskCompleteByIndexCommand());
            registry(new TaskChangeStatusByIdCommand());
            registry(new TaskChangeStatusByIndexCommand());
            registry(new TaskBindToProjectCommand());
            registry(new TaskUnbindFromProjectCommand());

            registry(new UserRegistryCommand());
            registry(new UserChangePasswordCommand());
            registry(new UserLoginCommand());
            registry(new UserLogoutCommand());
            registry(new UserUpdateProfileCommand());
            registry(new UserViewProfileCommand());
            registry(new UserLockCommand());
            registry(new UserUnlockCommand());
            registry(new UserRemoveCommand());

        }
        catch (@NotNull AbstractException e) {
            getLoggerService().error(e);
        }
    }

    private static void exit() {
        System.exit(0);
    }

    private void initDemoData() {
        userService.create("test", "test", "test@mail.ru");
        userService.create("test2", "test2", "test2@mail.ru");
        @NotNull User admin = userService.create("admin", "admin", Role.ADMIN);

        projectRepository.add(new Project("P4", "P1D", Status.IN_PROGRESS));
        projectRepository.add(new Project("P2", "P2D", Status.NOT_STARTED));
        projectRepository.add(new Project("P3", "P3D", Status.COMPLETED));
        projectRepository.add(new Project("P1", "P4D", Status.IN_PROGRESS));

        for (int i = 0; i < 10; i++) {
            @NotNull Task task = new Task(i + " TASK", "T" + i + "D");
            task.setUserId(admin.getId());
            taskRepository.add(task);
        }
    }

    private void initLogger() {
        loggerService.info("Task Manager started");
        Runtime.getRuntime().addShutdownHook(new Thread(() -> loggerService.info("Task Manager closed")));
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void run(@Nullable final String[] args) {
        if (processArgument(args)) exit();
        initDemoData();
        initLogger();

        processCommands();
    }

    private void processCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("\nEnter command:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[DONE]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private void processCommand(@NotNull final String command) throws CommandNotSupportedException {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException();
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void processArgument(@Nullable final String argument) throws ArgumentNotSupportedException {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException();
        abstractCommand.execute();
    }

    private boolean processArgument(@Nullable String[] args) {
        if (args == null || args.length < 1) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

}
