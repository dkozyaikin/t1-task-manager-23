package ru.t1.dkozyaikin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozyaikin.tm.enumerated.Status;
import ru.t1.dkozyaikin.tm.exception.AbstractException;
import ru.t1.dkozyaikin.tm.util.TerminalUtil;

public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME ="task-start-by-index";

    @NotNull
    public static final String DESCRIPTION = "Start task by index.";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[START TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final String userId = getAuthService().getUserId();
        getTaskService().changeTaskStatusByIndex(userId, index, Status.IN_PROGRESS);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
