package ru.t1.dkozyaikin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozyaikin.tm.enumerated.Role;
import ru.t1.dkozyaikin.tm.exception.AbstractException;
import ru.t1.dkozyaikin.tm.exception.user.AccessDeniedException;
import ru.t1.dkozyaikin.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "change-password";

    @NotNull
    public static final String DESCRIPTION = "Change password";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[CHANGE PASSWORD]");
        @NotNull final String userId = getAuthService().getUserId();
        System.out.println("ENTER NEW PASSWORD");
        @NotNull final String newPassword = TerminalUtil.nextLine();
        getUserService().setPassword(userId, newPassword);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
