package ru.t1.dkozyaikin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozyaikin.tm.api.service.IProjectService;
import ru.t1.dkozyaikin.tm.api.service.IProjectTaskService;
import ru.t1.dkozyaikin.tm.command.AbstractCommand;
import ru.t1.dkozyaikin.tm.enumerated.Role;
import ru.t1.dkozyaikin.tm.enumerated.Status;
import ru.t1.dkozyaikin.tm.model.Project;

public abstract class AbstractProjectCommand extends AbstractCommand {

    @NotNull
    public IProjectService getProjectService() {
        return getServiceLocator().getProjectService();
    }

    @NotNull
    protected IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    public void showProject(@Nullable Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId() + ";");
        System.out.println("Name: " + project.getName() + ";");
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + Status.toName(project.getStatus()));
        System.out.println("Created: " + project.getCreated());
    }

}
