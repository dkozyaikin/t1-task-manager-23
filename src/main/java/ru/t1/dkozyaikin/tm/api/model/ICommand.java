package ru.t1.dkozyaikin.tm.api.model;

import ru.t1.dkozyaikin.tm.exception.AbstractException;

public interface ICommand {

    String getArgument();

    String getDescription();

    String getName();

    void execute() throws AbstractException;

}
