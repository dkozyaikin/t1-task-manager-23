package ru.t1.dkozyaikin.tm.exception.entity;

public class ProjectNotFoundException extends AbstractEntityNotFoundException {

    public ProjectNotFoundException() {
        super("Error! Project not found in list...");
    }

}
